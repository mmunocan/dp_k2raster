#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <list_files> <output_path>"
	exit
fi

list_files=$1
output_path=$2

mkdir -p "$output_path/snaps_112"
mkdir -p "$output_path/snaps_111"

i=0
while IFS= read -r line
do
	set -- $line
		input_filename=$1
		input_path=$2
		raster_id=$3
		
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		nohup python3 get_snaps_bitmap_info.py "$output_path/heu_112/112_$raster_id-snaps.bin" "$output_path/dp_112/112_$raster_id-snaps.bin" > "$output_path/snaps_112/$output_result" &
		nohup python3 get_snaps_bitmap_info.py "$output_path/heu_111/111_$raster_id-snaps.bin" "$output_path/dp_111/111_$raster_id-snaps.bin" > "$output_path/snaps_111/$output_result" &
		
	i=$(($i + 1))
done < $list_files