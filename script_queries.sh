#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <list_files> <output_path>"
	exit
fi

list_files=$1
output_path=$2

mkdir -p $output_path
mkdir -p "$output_path/getCell"
mkdir -p "$output_path/windowsQuery"
mkdir -p "$output_path/rangeQuery"

mkdir -p "$output_path/getCell/heu_112"
mkdir -p "$output_path/getCell/heu_111"
mkdir -p "$output_path/getCell/dp_112"
mkdir -p "$output_path/getCell/dp_111"

mkdir -p "$output_path/windowsQuery/heu_112"
mkdir -p "$output_path/windowsQuery/heu_111"
mkdir -p "$output_path/windowsQuery/dp_112"
mkdir -p "$output_path/windowsQuery/dp_111"

mkdir -p "$output_path/rangeQuery/heu_112"
mkdir -p "$output_path/rangeQuery/heu_111"
mkdir -p "$output_path/rangeQuery/dp_112"
mkdir -p "$output_path/rangeQuery/dp_111"

i=0
while IFS= read -r line
do
	set -- $line
		raster_id=$1
		cell_file=$2
		region_file=$3
		
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		nohup ./build/bin/get_cell_tk2r "output/heu_112/112_$raster_id.tk2r" $cell_file > "$output_path/getCell/heu_112/$output_result" &
		nohup ./build/bin/get_cell_tk2r "output/heu_111/111_$raster_id.tk2r" $cell_file > "$output_path/getCell/heu_111/$output_result" &
		nohup ./build/bin/get_cell_tk2r "output/dp_112/112_$raster_id.tk2r" $cell_file > "$output_path/getCell/dp_112/$output_result" &
		nohup ./build/bin/get_cell_tk2r "output/dp_111/111_$raster_id.tk2r" $cell_file > "$output_path/getCell/dp_111/$output_result" &
		
		nohup ./build/bin/get_values_window_tk2r "output/heu_112/112_$raster_id.tk2r" $region_file 0 > "$output_path/windowsQuery/heu_112/$output_result" &
		nohup ./build/bin/get_values_window_tk2r "output/heu_111/111_$raster_id.tk2r" $region_file 0 > "$output_path/windowsQuery/heu_111/$output_result" &
		nohup ./build/bin/get_values_window_tk2r "output/dp_112/112_$raster_id.tk2r" $region_file 0 > "$output_path/windowsQuery/dp_112/$output_result" &
		nohup ./build/bin/get_values_window_tk2r "output/dp_111/111_$raster_id.tk2r" $region_file 0 > "$output_path/windowsQuery/dp_111/$output_result" &
		
		nohup ./build/bin/get_cells_by_value_tk2r "output/heu_112/112_$raster_id.tk2r" $region_file 0 > "$output_path/rangeQuery/heu_112/$output_result" &
		nohup ./build/bin/get_cells_by_value_tk2r "output/heu_111/111_$raster_id.tk2r" $region_file 0 > "$output_path/rangeQuery/heu_111/$output_result" &
		nohup ./build/bin/get_cells_by_value_tk2r "output/dp_112/112_$raster_id.tk2r" $region_file 0 > "$output_path/rangeQuery/dp_112/$output_result" &
		nohup ./build/bin/get_cells_by_value_tk2r "output/dp_111/111_$raster_id.tk2r" $region_file 0 > "$output_path/rangeQuery/dp_111/$output_result" &
		
	i=$(($i + 1))
done < $list_files