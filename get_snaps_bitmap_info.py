import sys
import numpy as np

if len(sys.argv) != 3:
	print("ERROR! USE " + sys.argv[0] + " <bitmap1> <bitmap2> ")
	sys.exit()

bitmap1 = sys.argv[1]
bitmap2 = sys.argv[2]

f = open(bitmap1, "rb")
data1 = np.fromfile(f, dtype=np.bool_)
f.close()
f = open(bitmap2, "rb")
data2 = np.fromfile(f, dtype=np.bool_)
f.close()

snapshots1 = 0
snapshots2 = 0
common = 0

distances1 = []
distances2 = []

distance1 = 0
distance2 = 0
for i in range(data1.shape[0]):
	if data1[i]:
		snapshots1 = snapshots1+1
		distances1.append(distance1)
		distance1 = 0
	else:
		distance1 = distance1+1
		
	if data2[i]:
		snapshots2 = snapshots2+1
		distances2.append(distance2)
		distance2 = 0
	else:
		distance2 = distance2+1
		
	if data1[i] and data2[i]:
		common = common+1
		
distances1 = np.array(distances1)
distances2 = np.array(distances2)

print("{};{};{};{};{};{};{};{};{}".format(bitmap1, bitmap2, snapshots1, snapshots2, common, distances1.min(), distances1.max(), distances2.min(), distances2.max()))

