/*  
 * Created by Fernando Silva on 19/9/19.
 *
 * Copyright (C) 2019-current-year, Fernando Silva, all rights reserved.
 *
 * 
 * Author's contact: Fernando Silva  <fernando.silva@udc.es>
 * Databases Lab, University of A Coruña. Campus de Elviña s/n. Spain
 *
 * DESCRIPTION
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef INCLUDED_AT_K2_RASTER_DP
#define INCLUDED_AT_K2_RASTER_DP

#include <omp.h>
#include <climits>

#include <k2_raster.hpp>
#include <temporal/k2_raster_temporal_base.hpp>
#include <temporal/helpers/t_k2_raster_log.hpp>
#include <temporal/helpers/t_k2_raster_plain_log.hpp>
#include <temporal/helpers/t_k2_raster_heuristic_log.hpp>

//! Namespace for k2-raster library
namespace k2raster {

template <typename t_value=int,
        typename t_k2_raster_t=k2_raster<t_value>,
        typename t_k2_raster_log_type=t_k2_raster_log<t_value>,
        typename t_bv=sdsl::bit_vector,
        typename t_rank_1=sdsl::rank_support_v5<1,1>>
class at_k2_raster_dp : public k2_raster_temporal_base<t_value> {

public:
    typedef t_k2_raster_t                           k2_raster_type; // k2_raster type
    typedef t_k2_raster_log_type                    k2_raster_log_type; // k2_raster type
    typedef k2_raster_temporal_base<>::size_type    size_type;
    typedef t_value                                 value_type;
    typedef t_bv                                    bit_vector_type;
    typedef t_rank_1                                rank_1_type;
public:
protected:
    size_type                       k_snapshots_freq;
    std::vector<k2_raster_type>     k_snapshots;
    std::vector<k2_raster_log_type> k_logs;

    /****** Structures DIFF ******/
    bit_vector_type                 k_is_snapshot;            // If a raster is a snapshot (1) or log (0)
    rank_1_type                     k_is_snapshot_rank1;      // rank support for 1-bits on m_ta

public:
    //*******************************************************//
    //******************* CONSTRUCTOR ***********************//
    //*******************************************************//
    at_k2_raster_dp() = default;
	
    at_k2_raster_dp(const at_k2_raster_dp &tr) {
        *this = tr;
    }

    at_k2_raster_dp(at_k2_raster_dp &&tr) {
        *this = std::move(tr);
    }

    at_k2_raster_dp(std::string &inputs_filename, std::string &input_path_folder, const ushort scale_factor) {	
		k_snapshots_freq = 1;
        this->k_k2_raster_type = AT_K2_RASTER_TYPE;

        if (typeid(k2_raster_type) == typeid(k2rh_type)) {
            this->k_k2_raster_type = ATH_K2_RASTER_TYPE;
        }
		
		this->k_n_times = build(inputs_filename, input_path_folder, scale_factor);
    }

    //*******************************************************//
    //*************** BASIC OPERATIONS **********************//
    //*******************************************************//

	inline std::vector<k2_raster_type> get_snapshots(){return k_snapshots;}
	inline std::vector<k2_raster_log_type> get_logs(){return k_logs;}
	
    //! Move assignment operator
    at_k2_raster_dp &operator=(at_k2_raster_dp &&tr) {
        if (this != &tr) {
            k2_raster_temporal_base<t_value>::operator=(tr);
            k_snapshots_freq = tr.k_snapshots_freq;
            k_snapshots = std::move(tr.k_snapshots);
            k_logs = std::move(tr.k_logs);
            k_is_snapshot = std::move(tr.k_is_snapshot);
            k_is_snapshot_rank1 = std::move(tr.k_is_snapshot_rank1);
            k_is_snapshot_rank1.set_vector(&k_is_snapshot);
        }
        return *this;
    }

    //! Assignment operator
    at_k2_raster_dp &operator=(const at_k2_raster_dp &tr) {
        if (this != &tr) {
            k2_raster_temporal_base<t_value>::operator=(tr);
            k_snapshots_freq = tr.k_snapshots_freq;
            k_snapshots = tr.k_snapshots;
            k_logs = tr.k_logs;
            k_is_snapshot = tr.k_is_snapshot;
            k_is_snapshot_rank1 = tr.k_is_snapshot_rank1;
            k_is_snapshot_rank1.set_vector(&k_is_snapshot);
        }
        return *this;
    }

    //! Swap operator
    void swap(at_k2_raster_dp &tr) {
        if (this != &tr) {
            k2_raster_temporal_base<t_value>::swap(tr);
            std::swap(k_snapshots_freq, tr.k_snapshots_freq);
            std::swap(k_snapshots, tr.k_snapshots);
            std::swap(k_logs, tr.k_logs);
            std::swap(k_is_snapshot, tr.k_is_snapshot);
            sdsl::util::swap_support(k_is_snapshot_rank1, tr.k_is_snapshot_rank1, &k_is_snapshot, &(tr.k_is_snapshot));
        }
    }

    //! Equal operator
    bool operator==(const at_k2_raster_dp &tr) const {
        if (!k2_raster_temporal_base<t_value>::operator==(tr)) {
            return false;
        }

        if (k_snapshots_freq != tr.k_snapshots_freq) {
            return false;
        }

        if ((k_snapshots.size() != tr.k_snapshots.size()) ||
            (k_logs.size() != tr.k_logs.size()) ) {
            return false;
        }

        if (k_is_snapshot.size() != tr.k_is_snapshot.size()) {
            return false;
        }
        return true;
    }

    //*******************************************************//
    //******************** QUERIES **************************//
    //*******************************************************//

    //*****************************//
    //********** GET CELL *********//
    //*****************************//
    value_type get_cell(size_type row, size_type col, size_type time) const {
        assert(time <= (this->k_n_times));

        size_type pos_snap = time == 0 ? 0 : k_is_snapshot_rank1(time);

        if ( k_is_snapshot[time] == 1) {
            // It is a snapshot
            return k_snapshots[pos_snap].get_cell(row, col);
        } else {
            // It is a log
            return k_logs[time - pos_snap].get_cell(k_snapshots[pos_snap - 1], row, col);
        }
    }

    //*****************************//
    //****** GET CELLS VALUES *****//
    //*****************************//
    size_type get_cells_by_value(size_type xini, size_type xend, size_type yini, size_type yend,
                                 value_type valini, value_type valend, size_type tmin, size_type tmax,
                                 std::vector<std::vector<std::pair<size_type, size_type>>> &result) {
        assert(tmin >= 0);
        assert(tmax <= (k_snapshots.size() + k_logs.size() -1));
        assert(tmin <= tmax);

        result.resize(tmax - tmin + 1);;
        size_type count_cells = 0;
        size_type pos_snap = tmin == 0 ? 0 : k_is_snapshot_rank1(tmin);
        for (auto t = tmin; t <= tmax; t++) {
            if ( k_is_snapshot[t] == 1) {
                // It is a snapshot
                count_cells += k_snapshots[pos_snap].get_cells_by_value(xini, xend, yini, yend, valini, valend, result[t - tmin]);
                pos_snap++;
            } else {
                // It is a log
                count_cells += k_logs[t - pos_snap].get_cells_by_value(k_snapshots[pos_snap-1],
                                                                       xini, xend, yini, yend, valini, valend, result[t - tmin]);
            }
        }
        return count_cells;
    }

    //*****************************//
    //***** GET VALUES WINDOW *****//
    //*****************************//
    size_type get_values_window(size_type xini, size_type xend, size_type yini, size_type yend,
                                size_type tmin, size_type tmax,
                                std::vector<std::vector<value_type>> &result) {
        assert(tmin >= 0);
        assert(tmax <= (k_snapshots.size() + k_logs.size() -1));
        assert(tmin <= tmax);

        result.resize(tmax - tmin + 1);;
        size_type count_cells = 0;
        size_type pos_snap = tmin == 0 ? 0 : k_is_snapshot_rank1(tmin);
        for (auto t = tmin; t <= tmax; t++) {
            if ( k_is_snapshot[t] == 1) {
                // It is a snapshot
                count_cells += k_snapshots[pos_snap].get_values_window(xini, xend, yini, yend, result[t - tmin]);
                pos_snap++;
            } else {
                // It is a log
                count_cells += k_logs[t - pos_snap].get_values_window(k_snapshots[pos_snap-1],
                                                                      xini, xend, yini, yend, result[t - tmin]);
            }
        }
        return count_cells;
    }

    //*****************************//
    //**** CHECK VALUES WINDOW ****//
    //*****************************//
    bool check_values_window(size_type xini, size_type xend, size_type yini, size_type yend,
                             value_type valini, value_type valend,
                             size_type tmin, size_type tmax, bool strong_check) {
        assert(tmin >= 0);
        assert(tmax <= (k_snapshots.size() + k_logs.size() -1));
        assert(tmin <= tmax);

        bool result;
        size_type pos_snap = tmin == 0 ? 0 : k_is_snapshot_rank1(tmin);
        for (auto t = tmin; t <= tmax; t++) {
            if ( k_is_snapshot[t] == 1) {
                // It is a snapshot
                result = k_snapshots[pos_snap].check_values_window(xini, xend, yini, yend, valini, valend, strong_check);
                pos_snap++;
            } else {
                // It is a log
                result = k_logs[t - pos_snap].check_values_window(k_snapshots[pos_snap-1],
                                                                  xini, xend, yini, yend, valini, valend, strong_check);
            }
            if (strong_check){
                if (!result) return false;
            } else {
                if (result) return true;
            }
        } // END FOR t
        return strong_check;
    }

    //*******************************************************//
    //********************** FILE ***************************//
    //*******************************************************//
    virtual size_type
    serialize(std::ostream &out, sdsl::structure_tree_node *v = nullptr, std::string name = "") const {

        sdsl::structure_tree_node *child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_type written_bytes = 0;

        /****** k2-raster temporal base ******/
        written_bytes += k2_raster_temporal_base<t_value>::serialize(out, child, name);


        /****** at k2-raster ******/
        written_bytes += write_member(k_snapshots_freq, out, child, "snapshots_freq");
        written_bytes += k_is_snapshot.serialize(out, child, "is_snapshot");
        written_bytes += k_is_snapshot_rank1.serialize(out, child, "is_snapshot_rank1");

        /****** Vector of snapshots ******/
        written_bytes += write_member(k_snapshots.size(), out, child, "num_snapshots");
        written_bytes +=sdsl::serialize_vector(k_snapshots, out, child, "snapshots");

        /****** Vector of logs ******/
        written_bytes += write_member(k_logs.size(), out, child, "num_logs");
        written_bytes +=sdsl::serialize_vector(k_logs, out, child, "logs");

        sdsl::structure_tree::add_size(child, written_bytes);
        return written_bytes;
    }

    virtual void load(std::istream &in) {
        /****** k2-raster base ******/
        k2_raster_temporal_base<t_value>::load(in);

        /****** t k2-raster ******/
        sdsl::read_member(k_snapshots_freq, in);
        k_is_snapshot.load(in);
        k_is_snapshot_rank1.load(in);
        k_is_snapshot_rank1.set_vector(&k_is_snapshot);

        /****** Vector of snapshots ******/
        size_t n;
        sdsl::read_member(n, in);
        k_snapshots.resize(n);
        sdsl::load_vector(k_snapshots, in);

        /****** Vector of logs ******/
        sdsl::read_member(n, in);
        k_logs.resize(n);
        sdsl::load_vector(k_logs, in);
		
    }

    void print_space_by_time() const {
        size_type times = k_snapshots.size() + k_logs.size();
        double size;
        double total_size = 0;
        std::string type;

        for (size_type t = 0; t < times; t++) {

            size_type pos_snap = t == 0 ? 0 : k_is_snapshot_rank1(t);
            if ( k_is_snapshot[t] == 0) {
                // It is a log
                size = sdsl::size_in_mega_bytes(k_logs[t - pos_snap]);
                type = "log";
            } else {
                // It is a snapshot
                size = sdsl::size_in_mega_bytes(k_snapshots[pos_snap]);
                type = "snapshot";
            }
            std::cout << "Time " << t << "(" << type << ") : " << std::setprecision(2) << std::fixed << size << " Mbs" << std::endl;
            total_size += size;
        }
        std::cout << "Total size times: " << std::setprecision(2) << std::fixed << total_size << " Mbs" << std::endl;
    };
	
protected:
    //*******************************************************//
    //******************* AUXILIARY *************************//
    //*******************************************************//
	
    //*****************************//
    //***** BUILD FUNCTIONS   *****//
    //*****************************//
    size_type build(std::string &inputs_filename, std::string &input_path_folder, const ushort scale_factor=0) {
		
		// Declaración de variables
		size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
        std::string file_path_input;
		size_t elem;
		
		k2_raster_log_type raster_log;
		k2_raster_type raster_snap_last;
		
		/**********************/
		/* Lectura de datos  */
		/**********************/
		std::ifstream inputs_file(inputs_filename);
        assert(inputs_file.is_open() && inputs_file.good());
		read_params_from_file(inputs_file, n_rows, n_cols, k1, k2, level_k1, plain_levels);
		// Obtención cantidad rasters
		size_type n_rasters;
        {
            std::ifstream file(inputs_filename);
            n_rasters = std::count(std::istreambuf_iterator<char>(file),
                       std::istreambuf_iterator<char>(), '\n') + 1;
            file.close();
        }
		
		std::vector<std::vector<int>> values(n_rasters, std::vector<int>(n_rows*n_cols));
		
		elem = 0;
		while (next_file_path(inputs_file, input_path_folder, file_path_input)) {
			read_input_data(file_path_input, n_rows, n_cols, values[elem], scale_factor);
			elem++;
		}
		inputs_file.close();
		
		// Declaración de más variables
		
		std::vector<k2_raster_type> k_snaps(elem);
		std::vector<long> ref(elem);
		std::vector<std::vector<long>> c(elem, std::vector<long>(elem, 0));
		std::vector<std::vector<long>> M(elem, std::vector<long>(elem, 0));
		std::vector<size_t> S;
		
		/**********************/
		/* Cálculo snapshots  */
		/**********************/
		#pragma omp parallel for
		for(size_t i = 0; i < elem; i++){
			k_snaps[i] = k2_raster_type(values[i], n_rows, n_cols, k1, k2, level_k1, plain_levels);    
			ref[i] = sdsl::size_in_bytes(k_snaps[i]);
		}
		
		/*****************/
		/* Cálculo logs  */
		/*****************/
		
		// Inicializacion de C
		#pragma omp parallel for
		for(size_t snap = 0; snap < elem; snap++){
			#pragma omp parallel for
			for(size_t log = snap+1; log < elem; log++){
				c[log][snap] = sdsl::size_in_bytes(k2_raster_log_type(values[log], k_snaps[snap]));
			}
		}

		/************************/
		/* Dynamic programming  */
		/************************/
		for(int i = 0; i < elem; i++){
			for(int j = 0; j <= i; j++){
				if(i == j){
					// Min_{k \in [0, i-1]} M[i-1,k]
					long min = LONG_MAX;
					int rep = 0;
					for(int k = 0; k <= j-1; k++){
						if(M[j-1][k] < min){
							min = M[j-1][k];
							rep++;
						}
					}
					if(rep == 0){
						min = 0;
					}
					
					M[i][j] = min + ref[i];
					
				}else if(j < i){
					// Min_{k \in [0, j]} M[i-1,k]
					long min = LONG_MAX;
					int rep = 0;
					for(int k = 0; k < j; k++){
						if(M[j-1][k] < min){
							min = M[j-1][k];
							rep++;
						}
					}
					if(rep == 0){
						min = 0;
					}
					// \Sum_{k=j+1}^{k <= i} c[k][j]
					long sum = 0;
					for(int k = j+1; k <= i; k++){
						sum += c[k][j];
					}
					
					M[i][j] = min + ref[j] + sum;
				}
			}
		}

		/**************/
		/* Compute S  */
		/**************/
		
		size_t row_revisted = elem-1;
		size_t event_s;
		while(row_revisted > 0){
			long min = LONG_MAX;
			for(size_t i = 0; i <= row_revisted; i++){
				if(M[row_revisted][i] < min){
					min = M[row_revisted][i];
					event_s = i;
				}
			}
			S.push_back(event_s);
			row_revisted = event_s != 0 ? event_s - 1 : 0;
		}
		S.push_back(0);
		
		
		/************************/
		/* raster construction  */
		/************************/
		
		k_is_snapshot.resize(elem);
		k_logs.clear();
        k_snapshots.clear();
		
		for(size_t i = 0; i < elem; i++){
			k_is_snapshot[i] = 0;
		}
		
		for(size_t i = 0; i < S.size(); i++){
			k_is_snapshot[S[i]] = 1;
		}
		
		this->k_max_size_x = 0;
        this->k_max_size_y = 0;
        this->k_min_value = std::numeric_limits<value_type>::max();
        this->k_max_value = std::numeric_limits<value_type>::min();
		
		for(size_t i = 0; i < elem; i++){
			
			/*********************/
			/* Global values     */
			/*********************/
			this->k_max_size_x = std::max(this->k_max_size_x, k_snaps[i].get_n_rows());
			this->k_max_size_y = std::max(this->k_max_size_y, k_snaps[i].get_n_cols());
			this->k_min_value = std::min(this->k_min_value, k_snaps[i].min_value);
			this->k_max_value = std::max(this->k_max_value, k_snaps[i].max_value);
			
			if(k_is_snapshot[i] == 1){
				k_snapshots.push_back(k_snaps[i]);
				raster_snap_last = k_snaps[i];
			}else{
				raster_log = k2_raster_log_type(values[i], raster_snap_last);
				k_logs.push_back(raster_log);
			}
		}
		
		sdsl::util::init_support(this->k_is_snapshot_rank1, &this->k_is_snapshot);
		
		return elem;
    }

    inline void store_raster_information(k2_raster_type &raster_snap, k2_raster_log_type &raster_log,
            k2_raster_type &raster_snap_prev, k2_raster_log_type &raster_log_prev, bool &has_prev_raster) {
        raster_log_prev = raster_log;
        raster_snap_prev = raster_snap;
        has_prev_raster = true;
    }
	
	
}; // END class t_k2_raster

// Types
    typedef k2raster::at_k2_raster_dp<> atk2r_type;
    typedef k2raster::at_k2_raster_dp<int, k2_raster_heuristic<>, t_k2_raster_heuristic_log<>> athk2r_type;
    typedef k2raster::at_k2_raster_dp<int, k2_raster_plain<>, t_k2_raster_plain_log<>> atpk2r_type;
} // END namespacek2raster

#endif // INCLUDED_T_K2_RASTER
