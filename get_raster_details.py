import sys
import numpy as np

if len(sys.argv) != 3:
	print("ERROR! USE " + sys.argv[0] + " <inputs_file> <inputs_path> ")
	sys.exit()

inputs_file = sys.argv[1]
inputs_path = sys.argv[2]

f = open(inputs_file, "r")
line1 = f.readline().split()
n_rows = int(line1[0])
n_cols = int(line1[1])

raster_type = np.dtype((np.int32, n_cols * n_rows))
rasters = []

n_temporal = 0

line1 = f.readline()
while line1:
	raster_filename = inputs_path + "/" + line1[:-1]
	file_raster = open(raster_filename, "rb")
	raster = np.fromfile(file_raster, dtype=raster_type)[0]
	file_raster.close()
	rasters.append(raster)
	line1 = f.readline()
	
	n_temporal = n_temporal + 1
	
f.close()


rasters = np.array(rasters)

rasters.resize(rasters.shape[0] * rasters.shape[1])

print("{};{};{};{};{};{};{}".format(inputs_file, n_rows, n_cols, n_temporal, rasters.min(), rasters.max(), np.unique(rasters).shape[0]))
